using classy.Network;


namespace classy.Packets.Client
{
    public class DisconnectPlayerPacket : IPacket
    {
        public string DisconnectReason { get; set; } = "";
        

        public void Decode(MinecraftStream minecraftStream)
        {
            DisconnectReason = minecraftStream.ReadString();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteString(DisconnectReason);
        }
    }
}