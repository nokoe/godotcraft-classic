using classy.Network;


namespace classy.Packets.Client
{
    public class SetBlockPacket : IPacket
    {
        public short X { get; set; }
        public short Y { get; set; }
        public short Z { get; set; }
        public byte BlockType { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            X = minecraftStream.ReadShort();
            Y = minecraftStream.ReadShort();
            Z = minecraftStream.ReadShort();
            BlockType = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteShort(X);
            minecraftStream.WriteShort(Y);
            minecraftStream.WriteShort(Z);
            minecraftStream.WriteByte(BlockType);
        }
    }
}