using classy.Network;


namespace classy.Packets.Client
{
    public class DespawnPlayerPacket : IPacket
    {
        public sbyte PlayerId { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            PlayerId = minecraftStream.ReadSByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteSByte(PlayerId);
        }
    }
}