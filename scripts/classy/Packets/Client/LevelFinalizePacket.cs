using classy.Network;


namespace classy.Packets.Client
{
    public class LevelFinalizePacket : IPacket
    {
        public short XSize { get; set; }
        public short YSize { get; set; }
        public short ZSize { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            XSize = minecraftStream.ReadShort();
            YSize = minecraftStream.ReadShort();
            ZSize = minecraftStream.ReadShort();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteShort(XSize);
            minecraftStream.WriteShort(YSize);
            minecraftStream.WriteShort(ZSize);
        }
    }
}