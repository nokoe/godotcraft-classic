using System;
using Godot;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using classy.Data;
using Godotcraft;
using System.Diagnostics;

public class TerrainGenerator : Spatial
{
    [Signal]
    delegate void ChunksReady();

    public static Dictionary<Vector3, Vector3[]> Faces = new Dictionary<Vector3, Vector3[]>(){
        {new Vector3(0, -1, 0), new Vector3[6] {new Vector3(1, 0, 0), new Vector3(1, 0, -1), new Vector3(0, 0, -1), new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 0, -1)}},
        {new Vector3(0, 1, 0), new Vector3[6] {new Vector3(0, 1, -1), new Vector3(1, 1, 0), new Vector3(0, 1, 0), new Vector3(0, 1, -1), new Vector3(1, 1, -1), new Vector3(1, 1, 0)}},
        {new Vector3(0, 0, -1), new Vector3[6] {new Vector3(1, 0, -1), new Vector3(1, 1, -1), new Vector3(0, 1, -1), new Vector3(0, 0, -1), new Vector3(1, 0, -1), new Vector3(0, 1, -1)}},
        {new Vector3(0, 0, 1), new Vector3[6] {new Vector3(0, 1, 0), new Vector3(1, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 1, 0), new Vector3(1, 1, 0), new Vector3(1, 0, 0)}},
        {new Vector3(-1, 0, 0), new Vector3[6] {new Vector3(0, 0, -1), new Vector3(0, 1, -1), new Vector3(0, 1, 0), new Vector3(0, 0, 0), new Vector3(0, 0, -1), new Vector3(0, 1, 0)}},
        {new Vector3(1, 0, 0), new Vector3[6] {new Vector3(1, 1, 0), new Vector3(1, 0, -1), new Vector3(1, 0, 0), new Vector3(1, 1, 0), new Vector3(1, 1, -1), new Vector3(1, 0, -1)}}
    };
    public static Dictionary<Vector3, Vector2[]> UvCoordinates = new Dictionary<Vector3, Vector2[]>(){
        {new Vector3(0, -1, 0), new Vector2[6] {new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1),new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1)}},
        {new Vector3(0, 1, 0), new Vector2[6] {new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, 0),new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)}},
        {new Vector3(0, 0, -1), new Vector2[6] {new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0),new Vector2(0, 1), new Vector2(1, 1), new Vector2(0, 0)}},
        {new Vector3(0, 0, 1), new Vector2[6] {new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1),new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1)}},
        {new Vector3(-1, 0, 0), new Vector2[6] {new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0),new Vector2(0, 1), new Vector2(1, 1), new Vector2(0, 0)}},
        {new Vector3(1, 0, 0), new Vector2[6] {new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1),new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1)}},
    };
    public static Block[] TransparentBlocks = { Block.Air, Block.Glass, Block.Leaves, Block.Dandelion, Block.Rose, Block.Sapling };
    public static Block[] PlantBlocks = { Block.Dandelion, Block.Rose, Block.Sapling };
    public Dictionary<Vector3, Spatial> Chunks = new Dictionary<Vector3, Spatial>();
    public List<Vector3> ChunksQueue = new List<Vector3>();
    public int ChunkSize = 16;
    public PackedScene plantScene = (PackedScene)ResourceLoader.Load("res://scenes/plant.tscn");
    public Node ChunksNode;
    public BlockTextures BlockTextures;
    public SpatialMaterial UnknownMaterial = new SpatialMaterial();
    private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
    private CancellationToken _cancellationToken;
    private int _totalChunks = 0;
    private int _chunksReady = 0;

    public override void _Ready()
    {
        ChunksNode = GetNode("Chunks");
        BlockTextures = GetNode<BlockTextures>("BlockTextures");
        _cancellationToken = _cancellationTokenSource.Token;
    }

    public void GenerateMap(Map map)
    {
        int chunksX = map.XSize / ChunkSize;
        int chunksY = map.YSize / ChunkSize;
        int chunksZ = map.ZSize / ChunkSize;
        _totalChunks = chunksX * chunksY * chunksZ;
        // Add chunks to list
        for (int x = 0; x < chunksX; x++)
        {
            for (int y = 0; y < chunksY; y++)
            {
                for (int z = 0; z < chunksZ; z++)
                {
                    Vector3 chunkPos = new Vector3(x * ChunkSize, y * ChunkSize, z * ChunkSize);
                    if (!ChunksQueue.Contains(chunkPos) && !Chunks.ContainsKey(chunkPos))
                    {
                        ArrayMesh arrayMesh = new ArrayMesh();
                        SurfaceTool surfaceTool = new SurfaceTool();
                        surfaceTool.Begin(Mesh.PrimitiveType.Triangles);
                        ThreadPool.QueueUserWorkItem(
                            new WaitCallback(delegate (object state)
                            {
                                this.GenerateChunk(chunkPos, map, arrayMesh, surfaceTool, _cancellationToken);
                            })
                        );
                        ChunksQueue.Add(chunkPos);
                    }
                }
            }
        }
    }

    public void GenerateChunk(Vector3 chunkPos, Map map, ArrayMesh arrayMesh, SurfaceTool st, CancellationToken _cancellationToken)
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        if (_cancellationToken.IsCancellationRequested)
        {
            return;
        }

        // Prepare chunk materials and add plants
        Dictionary<Block, SpatialMaterial> chunkBlockMaterials = new Dictionary<Block, SpatialMaterial>();
        Dictionary<Block, (SpatialMaterial, SpatialMaterial, SpatialMaterial)> multipleChunkBlockMaterials = new Dictionary<Block, (SpatialMaterial, SpatialMaterial, SpatialMaterial)>();
        Dictionary<Vector3, SpatialMaterial> plants = new Dictionary<Vector3, SpatialMaterial>();
        for (int x = 0; x < ChunkSize; x++)
        {
            for (int y = 0; y < ChunkSize; y++)
            {
                for (int z = 0; z < ChunkSize; z++)
                {
                    Block? maybeBlock = map.GetBlock((short)(x + chunkPos.x), (short)(y + chunkPos.y), (short)(z + chunkPos.z));
                    if (maybeBlock != null)
                    {
                        Block block = (Block)maybeBlock;
                        if (block != Block.Air && !Array.Exists(PlantBlocks, element => element == block))
                        {
                            if (!chunkBlockMaterials.ContainsKey(block))
                            {
                                if (BlockTextures.SingleBlockMaterials.ContainsKey(block))
                                {
                                    chunkBlockMaterials[block] = BlockTextures.SingleBlockMaterials[block];
                                }
                                else if (BlockTextures.MultipleBlockMaterials.ContainsKey(block))
                                {
                                    multipleChunkBlockMaterials[block] = BlockTextures.MultipleBlockMaterials[block];
                                }
                                else
                                {
                                    chunkBlockMaterials[block] = UnknownMaterial;
                                }
                            }
                        }
                        else if (Array.Exists(PlantBlocks, element => element == block))
                        {
                            plants[new Vector3(x, y, z)] = BlockTextures.SingleBlockMaterials.ContainsKey(block) ? BlockTextures.SingleBlockMaterials[block] : UnknownMaterial;
                        }
                    }
                }
            }
        }

        // Loop through chunk materials and create surfaces
        bool vertexAdded = true;
        if (chunkBlockMaterials.Count > 0)
        {
            foreach (KeyValuePair<Block, SpatialMaterial> kvp in chunkBlockMaterials)
            {
                Block currentBlock = kvp.Key;
                SpatialMaterial currentMaterial = kvp.Value;
                if (vertexAdded)
                {
                    st.Begin(Mesh.PrimitiveType.Triangles);
                    vertexAdded = false;
                }
                st.SetMaterial(currentMaterial);
                for (int x = 0; x < ChunkSize; x++)
                {
                    for (int y = 0; y < ChunkSize; y++)
                    {
                        for (int z = 0; z < ChunkSize; z++)
                        {
                            Block? maybeBlock = map.GetBlock((short)(x + chunkPos.x), (short)(y + chunkPos.y), (short)(z + chunkPos.z));
                            if (maybeBlock != null)
                            {
                                Block block = (Block)maybeBlock;
                                if (block == currentBlock)
                                {
                                    Vector3 blockPos = new Vector3(x + chunkPos.x, y + chunkPos.y, z + chunkPos.z);
                                    // Loop through all blocks next to this one and check if they are transparent
                                    foreach (Vector3 i in Faces.Keys)
                                    {
                                        Vector3 checkBlockPos = blockPos + i;
                                        if (!CheckBlock(map, checkBlockPos.x, checkBlockPos.y, checkBlockPos.z))
                                        {
                                            // Block is transparent, draw a face
                                            for (int v = 0; v < Faces[i].Length; v++)
                                            {
                                                st.AddUv(UvCoordinates[i][v]);
                                                st.AddVertex(Faces[i][v] + new Vector3(x, y, z));
                                            }
                                            vertexAdded = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (vertexAdded)
                {
                    st.GenerateNormals();
                    st.Commit(arrayMesh);
                    st.Clear();
                }
            }
        }

        // Add surfaces for blocks with multiple materials (e.g. grass block)
        if (multipleChunkBlockMaterials.Count > 0)
        {
            foreach (KeyValuePair<Block, (SpatialMaterial, SpatialMaterial, SpatialMaterial)> kvp in multipleChunkBlockMaterials)
            {
                Block currentBlock = kvp.Key;
                (SpatialMaterial, SpatialMaterial, SpatialMaterial) currentMaterials = kvp.Value;
                SpatialMaterial[] materials = new SpatialMaterial[3] { currentMaterials.Item1, currentMaterials.Item2, currentMaterials.Item3 };
                for (int i = 0; i < materials.Length; i++)
                {
                    SpatialMaterial currentMaterial = (SpatialMaterial)materials[i];
                    if (vertexAdded)
                    {
                        st.Begin(Mesh.PrimitiveType.Triangles);
                        vertexAdded = false;
                    }
                    st.SetMaterial(currentMaterial);
                    for (int x = 0; x < ChunkSize; x++)
                    {
                        for (int y = 0; y < ChunkSize; y++)
                        {
                            for (int z = 0; z < ChunkSize; z++)
                            {
                                Block? maybeBlock = map.GetBlock((short)(x + chunkPos.x), (short)(y + chunkPos.y), (short)(z + chunkPos.z));
                                if (maybeBlock != null)
                                {
                                    Block block = (Block)maybeBlock;
                                    if (block == currentBlock)
                                    {
                                        Vector3 blockPos = new Vector3(x + chunkPos.x, y + chunkPos.y, z + chunkPos.z);
                                        // Loop through all blocks next to this one and check if they are transparent
                                        Vector3[] neighbourBlocks;
                                        if (i == 0)
                                        {
                                            neighbourBlocks = new Vector3[] { Vector3.Up };
                                        }
                                        else if (i == 1)
                                        {
                                            neighbourBlocks = new Vector3[] { Vector3.Forward, Vector3.Back, Vector3.Left, Vector3.Right };
                                        }
                                        else
                                        {
                                            neighbourBlocks = new Vector3[] { Vector3.Down };
                                        }
                                        foreach (Vector3 direction in neighbourBlocks)
                                        {
                                            Vector3 checkBlockPos = blockPos + direction;
                                            if (!CheckBlock(map, checkBlockPos.x, checkBlockPos.y, checkBlockPos.z))
                                            {
                                                // Block is transparent, draw a face
                                                for (int v = 0; v < Faces[direction].Length; v++)
                                                {
                                                    st.AddUv(UvCoordinates[direction][v]);
                                                    st.AddVertex(Faces[direction][v] + new Vector3(x, y, z));
                                                }
                                                vertexAdded = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (vertexAdded)
                    {
                        st.GenerateNormals();
                        st.Commit(arrayMesh);
                        st.Clear();
                    }
                }
            }
        }

        // Create the final mesh instance and assign the mesh
        MeshInstance meshInstance = new MeshInstance();
        meshInstance.Mesh = arrayMesh;
        if (arrayMesh.GetSurfaceCount() > 0)
        {
            // For this to work, rendering/threads/thread_model needs to be set to "MultiThreaded" in the project settings.
            meshInstance.CreateTrimeshCollision();
        }
        if (_cancellationToken.IsCancellationRequested)
        {
            return;
        }
        // GD.Print($"Chunk generation time: {sw.ElapsedMilliseconds}");
        CallDeferred("ThreadSafeChunkLoading", meshInstance, chunkPos, plants);
        return;
    }

    public bool CheckBlock(Map map, float x, float y, float z)
    {
        Block? block = map.GetBlock((short)x, (short)y, (short)z);
        if (block == null)
            return false;
        else
            return (!Array.Exists(TransparentBlocks, element => element == block));
    }

    public void UpdateBlock(short x, short y, short z, Map map)
    {
        // Determine which chunk needs to be regenerated
        Vector3 targetChunk = new Vector3((int)((float)Math.Floor((double)x / (double)ChunkSize) * ChunkSize), (int)((float)Math.Floor((double)y / (double)ChunkSize) * ChunkSize), (int)((float)Math.Floor((double)z / (double)ChunkSize) * ChunkSize));
        // Check if chunk exists
        if (!Chunks.ContainsKey(targetChunk) && !ChunksQueue.Contains(targetChunk))
        {
            GD.Print($"Warning: Received SetBlock in non-existend chunk: {targetChunk.ToString()}");
            return;
        }
        // Regenerate chunk
        if (Chunks.ContainsKey(targetChunk))
        {
            GD.Print("Regenerating chunk");
            GenerateChunk(targetChunk, map, new ArrayMesh(), new SurfaceTool(), _cancellationToken);

            /*
            Alternative multi-threaded implementation:

            ArrayMesh arrayMesh = new ArrayMesh();
            SurfaceTool surfaceTool = new SurfaceTool();

            ThreadPool.QueueUserWorkItem(
                new WaitCallback(delegate (object state)
                {
                    this.GenerateChunk(targetChunk, map, arrayMesh, surfaceTool, _cancellationToken);
                })
            );
            */
        }
        // Check if bock is at a chunk border
        if ((x % ChunkSize) == 0 || (x % ChunkSize) == ChunkSize - 1 || (y % ChunkSize) == 0 || (y % ChunkSize) == ChunkSize - 1 || (z % ChunkSize) == 0 || (z % ChunkSize) == ChunkSize - 1)
        {
            int localX = x % ChunkSize;
            int localY = y % ChunkSize;
            int localZ = z % ChunkSize;
            Vector3 borderChunk = targetChunk;
            if (localX == 0)
            {
                borderChunk -= new Vector3(ChunkSize, 0, 0);
            }
            else if (localX == ChunkSize - 1)
            {
                borderChunk += new Vector3(ChunkSize, 0, 0);
            }
            if (Chunks.ContainsKey(borderChunk) && !ChunksQueue.Contains(borderChunk) && borderChunk != targetChunk)
            {
                // Regenerate chunk
                GD.Print($"Regenerating border chunk: {borderChunk}");
                GenerateChunk(borderChunk, map, new ArrayMesh(), new SurfaceTool(), _cancellationToken);
                borderChunk = targetChunk;
            }
            if (localY == 0)
            {
                borderChunk -= new Vector3(0, ChunkSize, 0);
            }
            else if (localY == ChunkSize - 1)
            {
                borderChunk += new Vector3(0, ChunkSize, 0);
            }
            if (Chunks.ContainsKey(borderChunk) && !ChunksQueue.Contains(borderChunk) && borderChunk != targetChunk)
            {
                // Regenerate chunk
                GD.Print($"Regenerating border chunk: {borderChunk}");
                GenerateChunk(borderChunk, map, new ArrayMesh(), new SurfaceTool(), _cancellationToken);
                borderChunk = targetChunk;
            }
            if (localZ == 0)
            {
                borderChunk -= new Vector3(0, 0, ChunkSize);
            }
            else if (localZ == ChunkSize - 1)
            {
                borderChunk += new Vector3(0, 0, ChunkSize);
            }
            if (Chunks.ContainsKey(borderChunk) && !ChunksQueue.Contains(borderChunk) && borderChunk != targetChunk)
            {
                // Regenerate chunk
                GD.Print($"Regenerating border chunk: {borderChunk}");
                GenerateChunk(borderChunk, map, new ArrayMesh(), new SurfaceTool(), _cancellationToken);
                borderChunk = targetChunk;
            }
        }
    }

    public void ThreadSafeChunkLoading(MeshInstance meshInstance, Vector3 chunkPos, Dictionary<Vector3, SpatialMaterial> plants)
    {
        if (ChunksQueue.Contains(chunkPos))
        {
            ChunksQueue.Remove(chunkPos);
        }
        if (Chunks.ContainsKey(chunkPos))
        {
            Spatial oldChunk = Chunks[chunkPos];
            GD.Print($"Removing old chunk at {oldChunk.Name}");
            oldChunk.QueueFree();
        }
        else
        {
            _chunksReady++;
            if (_chunksReady == _totalChunks)
            {
                GD.Print("All chunks ready.");
                EmitSignal("ChunksReady");
            }
        }
        ChunksNode.AddChild(meshInstance);
        meshInstance.Name = GD.Str(chunkPos);
        meshInstance.Translation = chunkPos;
        foreach (KeyValuePair<Vector3, SpatialMaterial> plantData in plants)
        {
            Plant plant = (Plant)plantScene.Instance();
            plant.Translation = plantData.Key;
            meshInstance.AddChild(plant);
            plant.SetPlantMaterial(plantData.Value);
        }
        Chunks[chunkPos] = meshInstance;
    }

    public override void _ExitTree()
    {
        _cancellationTokenSource.Cancel();
    }
}
