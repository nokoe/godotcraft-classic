using Godot;
using System;

public class Plant : StaticBody
{
    public void SetPlantMaterial(Material mat)
    {
        MeshInstance mesh1 = GetNode<MeshInstance>("Mesh1");
        MeshInstance mesh2 = GetNode<MeshInstance>("Mesh2");
        mesh1.MaterialOverride = mat;
        mesh2.MaterialOverride = mat;
    }
}
