extends Label


export var focus := true
export var shadow := true
export var auto_setup := true


func _ready():
	if auto_setup:
		set_up(Global.font, focus, shadow)


func set_up(label_font, label_focus, label_shadow):
	add_font_override("font", label_font)
	if label_focus:
		pass
		# add_color_override("font_color", Color("ffffff"))
		# add_color_override("font_color_shadow", Color("3f3f3f"))
	else:
		add_color_override("font_color", Color("a0a0a0"))
		add_color_override("font_color_shadow", Color("282828"))
	# add_constant_override("shadow_offset_x", 2)
	# add_constant_override("shadow_offset_y", 2)
	if not label_shadow:
		add_constant_override("shadow_offset_x", 0)
		add_constant_override("shadow_offset_y", 0)
		add_color_override("font_color_shadow", Color("003f3f3f"))
