extends Button


signal move_up
signal move_down
signal join_server

const STYLEBOX_NORMAL = preload("res://assets/styles/empty.tres")
const STYLEBOX_FOCUS = preload("res://assets/styles/text_field_normal.tres")

var server_name := ""
var server_address := ""
var server_info := {}
var thread

onready var no_ping = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 55, 10, 9)), 24, 24)
onready var ping1 = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 47, 10, 9)), 24, 24)
onready var ping2 = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 39, 10, 9)), 24, 24)
onready var ping3 = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 31, 10, 9)), 24, 24)
onready var ping4 = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 23, 10, 9)), 24, 24)
onready var ping5 = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/icons.png"), Rect2(0, 15, 10, 9)), 24, 24)
onready var play_texture = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(0, 0, 32, 32)), 72, 72)
onready var play_texture_hover = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(0, 32, 32, 32)), 72, 72)
onready var down_texture = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(64, 0, 32, 32)), 72, 72)
onready var down_texture_hover = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(64, 32, 32, 32)), 72, 72)
onready var up_texture = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(96, 0, 32, 32)), 72, 72)
onready var up_texture_hover = Global.get_resized_texture(Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/server_selection.png"), Rect2(96, 32, 32, 32)), 72, 72)

onready var title = $Title
onready var server_ping = $ServerPing
onready var server_icon = $ServerIcon
onready var hover_nodes = $HoverNodes
onready var play_icon = $HoverNodes/PlayIcon
onready var up_icon = $HoverNodes/UpIcon
onready var down_icon = $HoverNodes/DownIcon
onready var up_button = $HoverNodes/UpButton
onready var down_button = $HoverNodes/DownButton
onready var ping_icon = $PingIcon
onready var players = $Players
onready var motd_node = $Motd


func set_up(new_name, address):
	server_name = new_name
	add_stylebox_override("normal", STYLEBOX_NORMAL)
	add_stylebox_override("hover", STYLEBOX_NORMAL)
	add_stylebox_override("pressed", STYLEBOX_FOCUS)
	add_stylebox_override("focus", STYLEBOX_FOCUS)
	server_icon.texture = Global.get_resized_texture(Global.load_image_texture(Global.texture_dir + "misc/unknown_server.png"), 72, 72)
	ping_icon.texture = no_ping
	title.text = server_name
	play_icon.texture_normal = play_texture
	play_icon.texture_hover = play_texture_hover
	up_icon.texture = up_texture
	down_icon.texture = down_texture
	server_address = address
	motd_node.add_font_override("normal_font", Global.font)
	motd_node.bbcode_text = "[color=grey]Pinging...[/color]"
	thread = Thread.new()
	thread.start(self, "ping_server")


func refresh():
	if not thread.is_active():
		ping_icon.texture = no_ping
		motd_node.bbcode_text = "[color=grey]Pinging...[/color]"
		players.text = ""
		thread.start(self, "ping_server")


func ping_server():
	# Request server info
	print("Requesting %s server info" % server_address)
	server_ping.RequestServerInfo(server_address, 25565)


func _on_received_info(info_server_name: String, motd: String, ping: int):
	print("Received %s server info: %s, %s, Ping: %s" % [server_address, info_server_name, motd, ping])
	if ping == -1:
		ping_icon.texture = no_ping
		players.text = ""
		motd_node.bbcode_text = "[color=#aa0000]Can't connect to server[/color]"
		return
	elif ping > 400:
		ping_icon.texture = ping1
	elif ping > 300:
		ping_icon.texture = ping2
	elif ping > 200:
		ping_icon.texture = ping3
	elif ping > 100:
		ping_icon.texture = ping4
	else:
		ping_icon.texture = ping5
	# Parse motd
	motd = "[color=grey]" + info_server_name + "[/color]" + "\n[color=grey]" + motd + "[/color]"
	motd_node.bbcode_text = ""
	for i in ColorManager.GetColorCodes():
		motd = motd.replace(i, "[/color][color=#%s]" % ColorManager.GetForegroundColorHex(i))
	motd = motd.replace("lolb", "&b")
	var color_count = motd.count("[color=")
	var end_color_count = motd.count("[/color]")
	var diff = color_count - end_color_count
	if diff > 0:
		for i in diff:
			motd += "[/color]"
	motd_node.bbcode_text = motd
	call_deferred("finish_thread")


func finish_thread():
	if thread.is_active():
		thread.wait_to_finish()


func _exit_tree():
	finish_thread()


func _on_server_list_item_mouse_entered():
	hover_nodes.show()


func _on_server_list_item_mouse_exited():
	hover_nodes.hide()


func _process(_delta):
	if hover_nodes.visible:
		if up_button.is_hovered():
			up_icon.texture = up_texture_hover
			down_icon.texture = down_texture
		elif down_button.is_hovered():
			up_icon.texture = up_texture
			down_icon.texture = down_texture_hover
		else:
			up_icon.texture = up_texture
			down_icon.texture = down_texture


func _on_down_button_pressed():
	emit_signal("move_down", self)
	grab_focus()


func _on_up_button_pressed():
	emit_signal("move_up", self)
	grab_focus()


func _on_play_icon_pressed():
	emit_signal("join_server", server_address)
