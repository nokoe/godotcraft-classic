extends TextureButton


export var button_text : String
export var auto_setup := true


func _ready():
	if auto_setup:
		set_up()


func set_up():
	var normal_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 66), Vector2(200, 20)))
	var hover_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 86), Vector2(200, 20)))
	var disabled_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 46), Vector2(200, 20)))
	normal_texture = Global.get_resized_texture(normal_texture, normal_texture.get_size().x * 2 * Global.settings["gui_scale"], normal_texture.get_size().y * 2 * Global.settings["gui_scale"])
	hover_texture = Global.get_resized_texture(hover_texture, hover_texture.get_size().x * 2 * Global.settings["gui_scale"], hover_texture.get_size().y * 2 * Global.settings["gui_scale"])
	disabled_texture = Global.get_resized_texture(disabled_texture, disabled_texture.get_size().x * 2 * Global.settings["gui_scale"], disabled_texture.get_size().y * 2 * Global.settings["gui_scale"])
	texture_normal = normal_texture
	texture_hover = hover_texture
	texture_disabled = disabled_texture
	$text.add_font_override("font", Global.font)
	$text.text = button_text


func disable():
	# Grey out button
	disabled = true
	$text.add_color_override("font_color", Color("a0a0a0"))
	$text.add_color_override("font_color_shadow", Color("00ffffff"))


func enable():
	disabled = false
	$text.add_color_override("font_color", Color("ffffff"))
	$text.add_color_override("font_color_shadow", Color("3f3f3f"))
