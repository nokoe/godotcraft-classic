extends Button


export var button_text : String
export var button_size_x : int = 200
export var button_enabled := true
export var auto_setup := true

onready var part1 = $Parts/Part1
onready var part2 = $Parts/Part2
onready var text_node = $Text

var small_texture
var small_texture_end
var small_hover_texture
var small_hover_texture_end
var small_disabled_texture
var small_disabled_texture_end


func _ready():
	if auto_setup:
		set_up(button_size_x)


func set_up(size_x):
	rect_min_size.y = 40 * Global.settings["gui_scale"]
	rect_min_size.x = size_x * Global.settings["gui_scale"]
	small_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 66), Vector2(size_x * 0.5 - 4, 20)))
	small_texture_end = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(198, 66), Vector2(2, 20)))
	small_hover_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 86), Vector2(size_x * 0.5 - 4, 20)))
	small_hover_texture_end = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(198, 86), Vector2(2, 20)))
	small_disabled_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(0, 46), Vector2(size_x * 0.5 - 4, 20)))
	small_disabled_texture_end = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(Vector2(198, 46), Vector2(2, 20)))
	small_texture = Global.get_resized_texture(small_texture, small_texture.get_size().x * 2 * Global.settings["gui_scale"], small_texture.get_size().y * 2 * Global.settings["gui_scale"])
	small_texture_end = Global.get_resized_texture(small_texture_end, small_texture_end.get_size().x * 2 * Global.settings["gui_scale"], small_texture_end.get_size().y * 2 * Global.settings["gui_scale"])
	small_hover_texture = Global.get_resized_texture(small_hover_texture, small_hover_texture.get_size().x * 2 * Global.settings["gui_scale"], small_hover_texture.get_size().y * 2 * Global.settings["gui_scale"])
	small_hover_texture_end = Global.get_resized_texture(small_hover_texture_end, small_hover_texture_end.get_size().x * 2 * Global.settings["gui_scale"], small_hover_texture_end.get_size().y * 2 * Global.settings["gui_scale"])
	small_disabled_texture = Global.get_resized_texture(small_disabled_texture, small_disabled_texture.get_size().x * 2 * Global.settings["gui_scale"], small_disabled_texture.get_size().y * 2 * Global.settings["gui_scale"])
	small_disabled_texture_end = Global.get_resized_texture(small_disabled_texture_end, small_disabled_texture_end.get_size().x * 2 * Global.settings["gui_scale"], small_disabled_texture_end.get_size().y * 2 * Global.settings["gui_scale"])
	part1.texture = small_texture
	part2.texture = small_texture_end
	text_node.add_font_override("font", Global.font)
	text_node.text = button_text
	if not button_enabled:
		disable()


func disable():
	# Grey out button
	button_enabled = false
	part1.texture = small_disabled_texture
	part2.texture = small_disabled_texture_end
	text_node.add_color_override("font_color", Color("a0a0a0"))
	text_node.add_color_override("font_color_shadow", Color("00ffffff"))


func enable():
	button_enabled = true
	part1.texture = small_texture
	part2.texture = small_texture_end
	text_node.add_color_override("font_color", Color("ffffff"))
	text_node.add_color_override("font_color_shadow", Color("3f3f3f"))


func _on_scalable_button_mouse_entered():
	if button_enabled:
		part1.texture = small_hover_texture
		part2.texture = small_hover_texture_end


func _on_scalable_button_mouse_exited():
	if button_enabled:
		part1.texture = small_texture
		part2.texture = small_texture_end
