extends Node

signal music_finished

var autoplaying_menu_music = false
var autoplaying_survival_overworld_music = false
var menu_music_files = []
var survival_overworld_music_files = []
var current_music_index = 0

onready var music = $Music
onready var sound = $Sound


func autoplay_menu_music():
	randomize()
	autoplaying_menu_music = true
	menu_music_files = Global.list_files_in_dir(Global.music_dir + "menu/")
	current_music_index = randi() % menu_music_files.size()
	play_music(Global.load_sound(Global.music_dir + "menu/" + menu_music_files[current_music_index]))


func autoplay_survival_overworld_music():
	randomize()
	autoplaying_survival_overworld_music = true
	survival_overworld_music_files = Global.list_files_in_dir(Global.music_dir + "game/")
	for i in survival_overworld_music_files:
		if not i.ends_with(".ogg"):
			survival_overworld_music_files.erase(i)
	current_music_index = randi() % survival_overworld_music_files.size()
	play_music(Global.load_sound(Global.music_dir + "game/" + survival_overworld_music_files[current_music_index]))


func stop_menu_music():
	autoplaying_menu_music = false
	music.stop()


func stop_survival_overworld_music():
	autoplaying_survival_overworld_music = false
	music.stop()


func play_music(m):
	music.stream = m
	music.play()


func play_sound(s):
	sound.stream = s
	sound.play()


func _on_music_finished():
	emit_signal("music_finished")
	if autoplaying_survival_overworld_music:
		# Play a different menu music track
		var next_music_index = randi() % menu_music_files.size()
		while next_music_index == current_music_index:
			next_music_index = randi() % menu_music_files.size()
		current_music_index = next_music_index
		play_music(Global.load_sound(Global.music_dir + "menu/" + menu_music_files[current_music_index]))
	elif autoplay_survival_overworld_music():
		# Play a different survival overworld music track
		var next_music_index = randi() % survival_overworld_music_files.size()
		while next_music_index == current_music_index:
			next_music_index = randi() % survival_overworld_music_files.size()
		current_music_index = next_music_index
		play_music(Global.load_sound(Global.music_dir + "game/" + survival_overworld_music_files[current_music_index]))
