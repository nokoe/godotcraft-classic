extends Panel


const NORMAL_STYLE = preload("res://assets/styles/text_field_normal.tres")
const FOCUS_STYLE = preload("res://assets/styles/text_field_focus.tres")

export var enable_max_length := true
export var max_length := 32
export var auto_setup := true

var caret_visible = false
var focused = false


func _ready():
	if auto_setup:
		if enable_max_length:
			set_up(400, 40, max_length)
		else:
			set_up(400, 40, -1)


func set_up(size_x, size_y, text_max_length):
	rect_min_size.x = size_x * Global.settings["gui_scale"]
	rect_min_size.y = size_y * Global.settings["gui_scale"]
	$LineEdit.rect_size.y = rect_size.y
	$LineEdit.rect_size.x = rect_size.x - 10
	$LineEdit.rect_position.x = 10
	$text.rect_size.y = rect_size.y
	$text.rect_size.x = rect_size.x - 10
	$text.rect_position.x = 10
	$LineEdit.add_font_override("font", Global.font)
	$text.add_font_override("font", Global.font)
	add_stylebox_override("panel", NORMAL_STYLE)
	if text_max_length > 1:
		$LineEdit.max_length = text_max_length


func get_value() -> String:
	return $LineEdit.text


func set_text(text):
	$LineEdit.text = text


func _on_LineEdit_focus_entered():
	focused = true
	add_stylebox_override("panel", FOCUS_STYLE)


func _on_LineEdit_focus_exited():
	focused = false
	add_stylebox_override("panel", NORMAL_STYLE)
	$text.text = $LineEdit.text


func _process(_delta):
	if focused:
		if caret_visible:
			if $LineEdit.caret_position == $LineEdit.text.length():
				$text.text = $LineEdit.text.insert($LineEdit.caret_position, "_")
			else:
				$text.text = $LineEdit.text.insert($LineEdit.caret_position, "|")
		else:
			$text.text = $LineEdit.text


func reset_text():
	$LineEdit.text = ""
	$text.text = ""


func _on_caret_timer_timeout():
	if focused:
		caret_visible = !caret_visible
