extends KinematicBody


const GRAVITY = -32
const ACCEL = 5
const DEACCEL = 12
const AIR_ACCEL = 1
const AIR_DEACCEL = 2.5
var MAX_SPEED = 4.317
const SPRINT_MAX_SPEED = 5.612
const SPRINT_MAX_SPEED_JUMPING = 7.127
const JUMP_POWER = 10
const MOUSE_SENSITIVITY = 0.05
const DEFAULT_FOV = 70
const SPRINT_FOV = 80
const FOV_ACCELERATION = 6

var mouse_sensitivity_multiplier = 1
var target_fov = DEFAULT_FOV
var is_sprinting = false
var is_sneaking = false
var is_paused = false
var enabled := false
var dir = Vector3()
var vel = Vector3()

onready var multiplayer_node = get_parent()
onready var rotation_helper = $RotationHelper
onready var camera = $RotationHelper/Camera
onready var ingame_ui = $InGameUI
onready var place_break_ray_cast = $RotationHelper/Camera/PlaceBreakCast


func _physics_process(delta):
	var camera_transform = camera.get_global_transform()
	# Process input
	dir = Vector3()
	var input_movement_vector = Vector2()
	if not is_paused:
		if Input.is_action_pressed("movement_forward") or Input.is_action_pressed("movement_backward"):
			input_movement_vector.y = Input.get_action_strength("movement_forward") - Input.get_action_strength("movement_backward")
		if Input.is_action_pressed("movement_right") or Input.is_action_pressed("movement_left"):
			input_movement_vector.x = Input.get_action_strength("movement_right") - Input.get_action_strength("movement_left")
		input_movement_vector = input_movement_vector.normalized()
		
		if is_on_floor():
			if Input.is_action_pressed("movement_jump"):
				vel.y = JUMP_POWER
	dir += -camera_transform.basis.z * input_movement_vector.y
	dir += camera_transform.basis.x * input_movement_vector.x
	dir.y = 0
	dir = dir.normalized()
	# FOV
	if not Input.is_action_pressed("movement_sneak"):
		if Input.is_action_pressed("movement_sprint") and dir.z != 0:
			target_fov = SPRINT_FOV
			if not is_sprinting:
				is_sprinting = true
		else:
			target_fov = DEFAULT_FOV
			if is_sprinting:
				is_sprinting = false
	# Sneaking
	if Input.is_action_pressed("movement_sneak"):
		if is_sprinting:
			is_sprinting = false
		if not is_sneaking:
			is_sneaking = true
	elif is_sneaking:
		is_sneaking = false
	# Process movement
	vel.y += delta * GRAVITY
	var hvel = vel
	hvel.y = 0
	var target = dir
	if Input.is_action_pressed("movement_sprint"):
		if is_on_floor():
			target *= SPRINT_MAX_SPEED
		else:
			target *= SPRINT_MAX_SPEED_JUMPING
	else:
		target *= MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		if is_on_floor():
			accel = ACCEL
		else:
			accel = AIR_ACCEL
	else:
		if is_on_floor():
			accel = DEACCEL
		else:
			accel = AIR_DEACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	if enabled:
		vel = move_and_slide(vel, Vector3.UP, false, 4, deg2rad(45), true)
	else:
		pass
		#if $RayCastUp.is_colliding():
		#	enabled = true
		#	translation.y += 1
		#	$CollisionShape.disabled = false
		#elif $RayCastDown.is_colliding():
		#	enabled = true
		#	translation.y += 1
		#	$CollisionShape.disabled = false
	if translation.y < 0:
		translation.y = 256


func start():
	enabled = true
	translation.y += 5
	$CollisionShape.disabled = false


func _input(event):
	# Look around with mouse
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1 * mouse_sensitivity_multiplier))
		# Clamp rotation
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -89.9, 89.9)
		rotation_helper.rotation_degrees = camera_rot
	# Escape menu
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			ingame_ui.toggle_escape_menu(true)
			is_paused = true
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			ingame_ui.toggle_escape_menu(false)
			is_paused = false
	# Start game manually
	if Input.is_action_just_pressed("debug"):
		start()
	# Block placing / breaking
	if not is_paused:
		if Input.is_action_just_pressed("break"):
			if place_break_ray_cast.is_colliding():
				var collision_point = place_break_ray_cast.get_collision_point()
				var collision_normal = place_break_ray_cast.get_collision_normal() * -1
				var destination_block = floor_vector3(collision_point + collision_normal * 0.5) + Vector3(0, 0, 1)
				print("break block: %s, collison normal: %s" % [destination_block, collision_normal])
				multiplayer_node.break_block(destination_block)
		elif Input.is_action_just_pressed("place"):
			if place_break_ray_cast.is_colliding():
				var collision_point = place_break_ray_cast.get_collision_point()
				var collision_normal = place_break_ray_cast.get_collision_normal()
				var destination_block = floor_vector3(collision_point + collision_normal * 0.5) + Vector3(0, 0, 1)
				print("place block: %s, collison normal: %s" % [destination_block, collision_normal])
				multiplayer_node.place_block(destination_block)


func floor_vector3(pos: Vector3) -> Vector3:
	return Vector3(floor(pos.x), floor(pos.y), floor(pos.z))


func _process(delta):
	camera.fov = lerp(camera.fov, target_fov, FOV_ACCELERATION * delta)


func _on_IngameUI_close_escape_menu():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	ingame_ui.toggle_escape_menu(false)
	is_paused = false
