﻿Font “Gyroscope C” (Splash font) by Dmitriy Sychiov is licensed under a Creative Commons Attribution Share Alike license (http://creativecommons.org/licenses/by-sa/3.0/).
“Gyroscope C” was originally cloned (copied) from “gyroscope” by Christian Munk, which is licensed under a Creative Commons Attribution Share Alike license (http://creativecommons.org/licenses/by-sa/3.0/).
